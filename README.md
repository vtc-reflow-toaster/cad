## CAD Artifacts for Reflow Toaster

This repo contains assorted parts that were designed for the Reflow Toaster.

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
