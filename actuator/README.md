## Reflow Toaster Door Actuator

These parts are intended to be 3D printed and seat a 28BYJ-48 stepper motor.  A sheet of metal is intended to be mounted to the rack to hook down and catch the door, allowing it to be opened by the controller.

These parts need to be redesigned completely.  There are two major problems that must be addressed:
 - PLA softens at too low a temperature to be used here.  ABS may work better, but will likely require the parts to be redesigned to be strong enough.
 - This design cannot open the door wide enough.  A longer rack would help, but the actuator ultimately needs to be able to open the door all the way.
